<?php

namespace App\Form;

use App\Entity\Empresa;
use App\Repository\EmpresaRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class SociosType extends AbstractType
{
    /**
     * @var EmpresaRepository
     */
    private $empresaRepository;

    /**
     * @param EmpresaRepository $empresaRepository
     */
    public function __construct(EmpresaRepository $empresaRepository)
    {
        $this->empresaRepository = $empresaRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $empresas = $this->empresaRepository->findAll();

        $choices = [];
        foreach ($empresas as $empresa) {
            $choices[$empresa->getNome()] = $empresa->getId();
        }

        $builder
            ->add(
                'id_empresa',
                ChoiceType::class,
                [
                    'label' => 'Empresa:',
                    'choices' => $choices,
                    'placeholder' => 'Selecione a empresa',
                    'label_attr' => ['class' => 'col-2 text-right pr-2']
                ]
            )
            ->add(
                'cpf',
                TextType::class,
                [
                    'label' => 'CPF: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => [
                        'maxlength' => 11,
                        'class' => 'cpf'
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 11,
                            'exactMessage' => 'O campo {{ label }} tem que ter {{ limit }} caracteres',
                            'max' => 11,
                        ]),
                        new Regex([
                            'pattern' => '/^\d+$/',
                            'message' => 'O campo {{ label }} só permite caracteres inteiros'
                        ])
                    ]
                ]
            )->add(
                'nome',
                TextType::class,
                [
                    'label' => 'Nome do sócio: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => ['maxlength' => 250],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 3,
                            'minMessage' => 'O campo {{ label }} tem que ter, no mínimo, {{ min }} caracteres',
                            'maxMessage' => 'O campo {{ label }} tem que ter, no máximo, {{ max }} caracteres',
                            'max' => 250,
                        ])
                    ]
                ]
            )->add(
                'sexo',
                ChoiceType::class,
                [
                    'label' => 'Sexo:',
                    'choices' => ['Masculino' => 'M', 'Feminino' => 'F'],
                    'placeholder' => 'Selecione',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 1,
                            'exactMessage' => 'O campo {{ label }} tem que ter {{ min }} caracteres',
                            'max' => 1,
                        ])
                    ]
                ]
            )->add(
                'Salvar',
                SubmitType::class,
                ['attr' => ['class' => 'btn btn-primary text-right ']]
            );
    }
}