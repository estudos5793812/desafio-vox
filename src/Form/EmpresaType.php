<?php

namespace App\Form;

use App\Repository\UFRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class EmpresaType extends AbstractType
{

    protected $ufRepository;

    public function __construct(UFRepository $ufRepository)
    {
        $this->ufRepository = $ufRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options) : void
    {
        $ufs = $this->ufRepository->findAll();

        $choices = [];
        foreach ($ufs as $uf) {
            $choices[$uf->getNome()] = $uf->getSigla();
        }

        $builder
            ->add(
                'cnpj',
                TextType::class,
                [
                    'label' => 'CNPJ: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => [
                        'maxlength' => 14,
                        'class' => 'cnpj'
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 14,
                            'exactMessage' => 'O campo {{ label }} tem que ter {{ limit }} caracteres',
                            'max' => 14,
                        ]),
                        new Regex([
                            'pattern' => '/^\d+$/',
                            'message' => 'O campo {{ label }} só permite caracteres inteiros'
                        ])
                    ]
                ]
            )->add(
                'nome',
                TextType::class,
                [
                    'label' => 'Nome da empresa: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => ['maxlength' => 250],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 3,
                            'minMessage' => 'O campo {{ label }} tem que ter, no mínimo, {{ min }} caracteres',
                            'maxMessage' => 'O campo {{ label }} tem que ter, no máximo, {{ max }} caracteres',
                            'max' => 250,
                        ])
                    ]
                ]
            )->add(
                'endereco',
            TextType::class,
                [
                    'label' => 'Endereço: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => ['maxlength' => 250],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 3,
                            'minMessage' => 'O campo {{ label }} tem que ter, no mínimo, {{ min }} caracteres',
                            'maxMessage' => 'O campo {{ label }} tem que ter, no máximo, {{ max }} caracteres',
                            'max' => 250,
                        ])
                    ]
                ]
            )->add(
                'cidade',
                TextType::class,
                [
                    'label' => 'Cidade: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => ['maxlength' => 50],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 3,
                            'minMessage' => 'O campo {{ label }} tem que ter, no mínimo, {{ min }} caracteres',
                            'maxMessage' => 'O campo {{ label }} tem que ter, no máximo, {{ max }} caracteres',
                            'max' => 50,
                        ])
                    ]
                ]
            )->add(
                'estado',
                ChoiceType::class,
                [
                    'label' => 'Estado:',
                    'choices' => $choices,
                    'placeholder' => 'Selecione o Estado',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Length([
                            'min' => 2,
                            'exactMessage' => 'O campo {{ label }} tem que ter {{ limit }} caracteres',
                            'max' => 2,
                        ])
                    ]
                ]
            )->add(
                'cep',
                TextType::class,
                [
                    'label' => 'CEP: ',
                    'label_attr' => ['class' => 'col-2 text-right pr-2'],
                    'attr' => [
                        'maxlength' => 8,
                        'class' => 'cep'
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'O campo {{ label }} não pode ser vazio.',
                        ]),
                        new Regex([
                            'pattern' => '/^\d+$/',
                            'message' => 'O campo {{ label }} só permite caracteres inteiros'
                        ]),
                        new Length([
                            'min' => 8,
                            'exactMessage' => 'O campo {{ label }} tem que ter {{ limit }} caracteres',
                            'max' => 8,
                        ])
                    ]
                ]
            )->add(
                'Salvar',
                SubmitType::class,
                ['attr' => ['class' => 'btn btn-primary text-right ']]
            );
    }
}