<?php

namespace App\Repository;

use App\Entity\Empresa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Empresa>
 *
 * @method Empresa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Empresa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Empresa[]    findAll()
 * @method Empresa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmpresaRepository extends ServiceEntityRepository
{
    protected $emi;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $emi)
    {
        $this->emi = $emi;
        parent::__construct($registry, Empresa::class);
    }

    /**
     * @return Empresa[]
     */
    public function findAllEmpresas():array
    {
        return $this->emi->getRepository(Empresa::class)->findAll();
    }
}
