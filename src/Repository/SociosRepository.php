<?php

namespace App\Repository;

use App\Entity\Socios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Socios>
 *
 * @method Socios|null find($id, $lockMode = null, $lockVersion = null)
 * @method Socios|null findOneBy(array $criteria, array $orderBy = null)
 * @method Socios[]    findAll()
 * @method Socios[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SociosRepository extends ServiceEntityRepository
{
    protected $emi;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $emi)
    {
        $this->emi = $emi;
        parent::__construct($registry, Socios::class);
    }

    /**
    * @return Socios[]
    */
    public function findAllSocios()
    {
        $conn = $this->emi->getConnection();

        $sql = '
            SELECT s.*, e.nome as empresa FROM socios s
             JOIN empresa e on e.id = s.id_empresa
            ORDER BY s.nome ASC
            ';

        $resultSet = $conn->executeQuery($sql);

        return $resultSet->fetchAllAssociative();

    }
}
