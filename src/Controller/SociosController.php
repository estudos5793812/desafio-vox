<?php

namespace App\Controller;

use App\Entity\Socios;
use App\Form\SociosType;
use App\Repository\EmpresaRepository;
use App\Repository\SociosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SociosController extends AbstractController
{

    #[Route('/cadastrar/socio')]
    public function cadastrar(
        Request $request,
        EntityManagerInterface $emi
    ) {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $socio = new Socios();
        $form = $this->createForm(SociosType::class, $socio);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $emi->persist($socio);
            $emi->flush();
            $this->addFlash(
                'success',
                'Sócio cadastrado com sucesso!'
            );
            return $this->redirectToRoute('app_socios_listar');
        }

        return $this->render('socios/cadastrar.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/listar/socio')]
    public function listar(
        SociosRepository $sociosRepository,
        Request $request
    ) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $socios = $sociosRepository->findAllSocios();

        return $this->render('socios/listar.html.twig', [
            'socios'=> $socios,
            'mensagem' => null
        ]);
    }

    #[Route('/editar/socio/{id}')]
    public function editar(
        int $id,
        Request $request,
        EntityManagerInterface $emi,
        SociosRepository $sociosRepository
    ) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $socios = $sociosRepository->find($id);
        $form = $this->createForm(SociosType::class, $socios);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $emi->flush();
            $this->addFlash(
                'success',
                'Alterações realizadas com sucesso!'
            );

            return $this->redirectToRoute('app_socios_listar');
        }

        return $this->render('socios/editar.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/excluir/socio/{id}')]
    public function excluir(
        int $id,
        EntityManagerInterface $emi,
        SociosRepository $sociosRepository
    ) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $socios = $sociosRepository->find($id);
        $emi->remove($socios);
        $emi->flush();
        $this->addFlash(
            'success',
            'Sócio excluído com sucesso!'
        );

        return $this->redirectToRoute('app_socios_listar');
    }

    #[Route('/visualizar/{id}')]
    public function visualizar(
        int $id,
        EmpresaRepository $empresaRepository,
        SociosRepository $sociosRepository
    ): Response
    {
        $socio = $sociosRepository->find($id);
        $empresa = $empresaRepository->find($socio->getIdEmpresa());

        return $this->render('socios/visualizar.html.twig', [
            'empresa'=> $empresa,
            'socio' => $socio
        ]);
    }

}