<?php

namespace App\Controller;

use App\Entity\Empresa;
use App\Form\EmpresaType;
use App\Repository\EmpresaRepository;
use App\Repository\SociosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmpresaController extends AbstractController
{
    #[Route('/empresa/cadastrar')]
    public function cadastrar(
        Request $request,
        EntityManagerInterface $emi
    ) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $empresa = new Empresa();
        $form = $this->createForm(EmpresaType::class, $empresa);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $emi->persist($empresa);
            $emi->flush();
            $this->addFlash(
                'success',
                'Empresa cadastrada com sucesso!'
            );

            return $this->redirectToRoute('app_empresa_listar');
        }

        return $this->render('empresa/cadastrar.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/empresa/listar')]
    public function listar(
        EmpresaRepository $empresaRepository
    ): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $empresas = $empresaRepository->findAllEmpresas();

        return $this->render('empresa/listar.html.twig', [
            'empresas'=> $empresas,
        ]);
    }

    #[Route('/editar/empresa/{id}')]
    public function editar(
        int $id,
        Request $request,
        EntityManagerInterface $emi,
        EmpresaRepository $empresaRepository
    ) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $empresa = $empresaRepository->find($id);
        $form = $this->createForm(EmpresaType::class, $empresa);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $emi->flush();
            $this->addFlash(
                'success',
                'Alterações realizadas com sucesso!'
            );

            return $this->redirectToRoute('app_empresa_listar');
        }

        return $this->render('empresa/editar.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/excluir/empresa/{id}')]
    public function excluir(
        int $id,
        EntityManagerInterface $emi,
        EmpresaRepository $empresaRepository,
        SociosRepository $sociosRepository
    ) : Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $empresa = $empresaRepository->find($id);

        $socios = $sociosRepository->findBy(['idEmpresa' => $empresa->getId()]);
        foreach ($socios as $socio) {
            $emi->remove($socio);
        }

        $emi->remove($empresa);
        $emi->flush();

        $this->addFlash(
            'success',
            'Empresa excluída com sucesso!'
        );

        return $this->redirectToRoute('app_empresa_listar');
    }

    #[Route('/visualizar/{id}')]
    public function visualizar(
        int $id,
        EmpresaRepository $empresaRepository,
        SociosRepository $sociosRepository
    ): Response
    {
        $empresa = $empresaRepository->find($id);
        $socios = $sociosRepository->findBy(['idEmpresa' => $empresa->getId()]);

        return $this->render('empresa/visualizar.html.twig', [
            'empresa'=> $empresa,
            'socios' => $socios
        ]);
    }
}