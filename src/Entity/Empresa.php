<?php

namespace App\Entity;

use App\Repository\EmpresaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraint as Assert;

#[ORM\Entity(repositoryClass: EmpresaRepository::class)]
class Empresa
{
    #[ORM\Id, ORM\GeneratedValue(strategy: "AUTO"), ORM\Column(type: "integer")]
    protected ?int $id = null;

    #[ORM\Column(name: 'cnpj', nullable: false, unique: true, length: 14)]
    protected string $cnpj;

    #[ORM\Column(name: 'nome', nullable: false, unique: false, length: 250)]
    protected string $nome;

    #[ORM\Column(name: 'endereco', nullable: false, unique: false, length: 250)]
    protected string $endereco;

    #[ORM\Column(name: 'cidade', nullable: false, unique: false, length: 50)]
    protected string $cidade;

    #[ORM\Column(name: 'estado', nullable: false, unique: false, length: 2)]
    protected string $estado;

    #[ORM\Column(name: 'cep', nullable: false, unique: false, length: 8)]
    protected string $cep;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    public function setCnpj(string $cnpj): void
    {
        $this->cnpj = $cnpj;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    public function setEndereco(string $endereco): void
    {
        $this->endereco = $endereco;
    }

    public function getCidade(): ?string
    {
        return $this->cidade;
    }

    public function setCidade(string $cidade): void
    {
        $this->cidade = $cidade;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): void
    {
        $this->estado = $estado;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(string $cep): void
    {
        $this->cep = $cep;
    }
}
