<?php

namespace App\Entity;

use App\Repository\MunicipioRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MunicipioRepository::class)]
class Municipio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $id_uf = null;

    #[ORM\Column(length: 50)]
    private ?string $nome = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUf(): ?int
    {
        return $this->id_uf;
    }

    public function setIdUf(int $id_uf): static
    {
        $this->id_uf = $id_uf;

        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): static
    {
        $this->nome = $nome;

        return $this;
    }
}
