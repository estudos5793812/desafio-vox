<?php

namespace App\Entity;

use App\Repository\UFRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UFRepository::class)]
class UF
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 25)]
    private ?string $nome = null;

    #[ORM\Column(length: 2)]
    private ?string $sigla = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): static
    {
        $this->nome = $nome;

        return $this;
    }

    public function getSigla(): ?string
    {
        return $this->sigla;
    }

    public function setSigla(string $sigla): static
    {
        $this->sigla = $sigla;

        return $this;
    }
}
