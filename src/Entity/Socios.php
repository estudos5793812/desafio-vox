<?php

namespace App\Entity;

use App\Repository\SociosRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SociosRepository::class)]
class Socios
{
    #[ORM\Id, ORM\GeneratedValue(strategy: "AUTO"), ORM\Column(type: "integer")]
    private ? int $id = null;

    #[ORM\Column(name: 'id_empresa', nullable: false, unique: false)]
    private ? int $idEmpresa;

    #[ORM\Column(name: 'nome', nullable: false, unique: false, length: 250)]
    private ? string $nome;

    #[ORM\Column(name: 'cpf', nullable: false, unique: false, length: 11)]
    private ? int $cpf;

    #[ORM\Column(name: 'sexo', nullable: false, unique: false, length: 1)]
    private ? string $sexo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getIdEmpresa(): ?int
    {
        return $this->idEmpresa;
    }

    public function setIdEmpresa(?int $idEmpresa): void
    {
        $this->idEmpresa = $idEmpresa;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): void
    {
        $this->nome = $nome;
    }

    public function getCpf(): ?int
    {
        return $this->cpf;
    }

    public function setCpf(?int $cpf): void
    {
        $this->cpf = $cpf;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(?string $sexo): void
    {
        $this->sexo = $sexo;
    }

}
