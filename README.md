## Acessando o projeto

1. Faça um clone deste repositório Git: https://gitlab.com/estudos5793812/desafio-vox;
2. Acesse o terminal e entre no diretório onde se encontra o projeto desafioVOX;
3. Rode o comando `composer install`;
4. Agora digite o comando `symfony serve` para levantar o servidor do Symfony;
5. Configure, no arquivo `.env` , a conexão com o banco de dados Postgres através da constate DATABASE_URL;
6. Em seguida, rode o migrate para criar as tabelas do banco de dados:

   `php bin/console doctrine:migrations:migrate DoctrineMigrations\Version20240323014321`

7. Em seguida, devem-se executar os dois scripts que estão na pasta `public/scripts`, chamados de 'estados.sql' e 'municipios.sql';
8. Agora, acesse a URL do symfony serve em seu navegador de internet, como, por exemplo:  http://127.0.0.1:8000/;
9. Com isso, você conseguirá acessar a página principal, com as opções Entrar e Criar nova conta. Nesta página há a mensagem "Página de cadastramento e gerenciamento de empresas e sócios."
10. Após criado o novo usuário, deve-se entrar com login e senha para ter acesso às opções Empresa, Sócios e Sair.