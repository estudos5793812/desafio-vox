<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240321002904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE empresa_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE socios_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE empresa (id SERIAL NOT NULL, nome VARCHAR(100) DEFAULT NULL, cnpj VARCHAR(14) DEFAULT NULL, endereco VARCHAR(255) DEFAULT NULL, cidade VARCHAR(50) DEFAULT NULL, estado VARCHAR(2) DEFAULT NULL, cep VARCHAR(8) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE socios (id SERIAL NOT NULL, id_empresa INT DEFAULT NULL, nome VARCHAR(100) DEFAULT NULL, cpf VARCHAR(11) DEFAULT NULL, sexo CHAR(1) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_62EAC1FC664AF320 ON socios (id_empresa)');
        $this->addSql('ALTER TABLE socios ADD CONSTRAINT socios_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES empresa (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE empresa_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE socios_id_seq CASCADE');
        $this->addSql('ALTER TABLE socios DROP CONSTRAINT socios_id_empresa_fkey');
        $this->addSql('DROP TABLE empresa');
        $this->addSql('DROP TABLE socios');
    }
}
