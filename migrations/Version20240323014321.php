<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240323014321 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE empresa_id_seq1 CASCADE');
        $this->addSql('DROP SEQUENCE socios_id_seq1 CASCADE');
        $this->addSql('CREATE SEQUENCE municipio_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE uf_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE usuario_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE municipio (id INT NOT NULL, id_uf INT NOT NULL, nome VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE uf (id INT NOT NULL, nome VARCHAR(25) NOT NULL, sigla VARCHAR(2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE usuario (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, nome VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_IDENTIFIER_EMAIL ON usuario (email)');
        $this->addSql('ALTER TABLE empresa ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE empresa ALTER nome SET NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER nome TYPE VARCHAR(250)');
        $this->addSql('ALTER TABLE empresa ALTER cnpj SET NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER endereco SET NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER endereco TYPE VARCHAR(250)');
        $this->addSql('ALTER TABLE empresa ALTER cidade SET NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER estado SET NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER cep TYPE VARCHAR(8)');
        $this->addSql('ALTER TABLE empresa ALTER cep SET NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B8D75A50C8C6906B ON empresa (cnpj)');
        $this->addSql('ALTER TABLE socios DROP CONSTRAINT socios_id_empresa_fkey');
        $this->addSql('DROP INDEX IDX_62EAC1FC664AF320');
        $this->addSql('ALTER TABLE socios ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE socios ALTER id_empresa SET NOT NULL');
        $this->addSql('ALTER TABLE socios ALTER nome SET NOT NULL');
        $this->addSql('ALTER TABLE socios ALTER nome TYPE VARCHAR(250)');
        $this->addSql('ALTER TABLE socios ALTER cpf TYPE VARCHAR(11)');
        $this->addSql('ALTER TABLE socios ALTER cpf SET NOT NULL');
        $this->addSql('ALTER TABLE socios ALTER sexo TYPE VARCHAR(1)');
        $this->addSql('ALTER TABLE socios ALTER sexo SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE municipio_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE uf_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE usuario_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE empresa_id_seq1 INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE socios_id_seq1 INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE municipio');
        $this->addSql('DROP TABLE uf');
        $this->addSql('DROP TABLE usuario');
        $this->addSql('DROP INDEX UNIQ_B8D75A50C8C6906B');
        $this->addSql('CREATE SEQUENCE empresa_id_seq');
        $this->addSql('SELECT setval(\'empresa_id_seq\', (SELECT MAX(id) FROM empresa))');
        $this->addSql('ALTER TABLE empresa ALTER id SET DEFAULT nextval(\'empresa_id_seq\')');
        $this->addSql('ALTER TABLE empresa ALTER cnpj DROP NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER nome DROP NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER nome TYPE VARCHAR(100)');
        $this->addSql('ALTER TABLE empresa ALTER endereco DROP NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER endereco TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE empresa ALTER cidade DROP NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER estado DROP NOT NULL');
        $this->addSql('ALTER TABLE empresa ALTER cep TYPE VARCHAR(8)');
        $this->addSql('ALTER TABLE empresa ALTER cep DROP NOT NULL');
        $this->addSql('CREATE SEQUENCE socios_id_seq');
        $this->addSql('SELECT setval(\'socios_id_seq\', (SELECT MAX(id) FROM socios))');
        $this->addSql('ALTER TABLE socios ALTER id SET DEFAULT nextval(\'socios_id_seq\')');
        $this->addSql('ALTER TABLE socios ALTER id_empresa DROP NOT NULL');
        $this->addSql('ALTER TABLE socios ALTER nome DROP NOT NULL');
        $this->addSql('ALTER TABLE socios ALTER nome TYPE VARCHAR(100)');
        $this->addSql('ALTER TABLE socios ALTER cpf TYPE VARCHAR(11)');
        $this->addSql('ALTER TABLE socios ALTER cpf DROP NOT NULL');
        $this->addSql('ALTER TABLE socios ALTER sexo TYPE CHAR(1)');
        $this->addSql('ALTER TABLE socios ALTER sexo DROP NOT NULL');
        $this->addSql('ALTER TABLE socios ADD CONSTRAINT socios_id_empresa_fkey FOREIGN KEY (id_empresa) REFERENCES empresa (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_62EAC1FC664AF320 ON socios (id_empresa)');
    }
}
